package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.command.AbstractUserCommand;
import ru.yaleksandrova.tm.model.User;

public class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        showUser(user);
    }

}
