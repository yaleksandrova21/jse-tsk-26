package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class UserByLoginLockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = ApplicationUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
