package ru.yaleksandrova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        @NotNull final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = ApplicationUtil.nextLine();
        final Task taskUpdatedIndex = serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
        if (taskUpdatedIndex == null) {
            return;
        }
        System.out.println("[Task updated]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
