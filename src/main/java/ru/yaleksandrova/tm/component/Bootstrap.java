package ru.yaleksandrova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.yaleksandrova.tm.api.repository.IAuthRepository;
import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.api.repository.IUserRepository;
import ru.yaleksandrova.tm.api.sevice.*;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.command.project.*;
import ru.yaleksandrova.tm.command.system.*;
import ru.yaleksandrova.tm.command.task.*;
import ru.yaleksandrova.tm.command.user.*;
import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.system.UnknownCommandException;
import ru.yaleksandrova.tm.repository.*;
import ru.yaleksandrova.tm.service.*;
import ru.yaleksandrova.tm.util.ApplicationUtil;
import ru.yaleksandrova.tm.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final CommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);


    public void start(@Nullable final String[] args) {
        System.out.println("** Welcome to Task Manager **");
        initCommands();
        initUsers();
        parseArgs(args);
        initPID();
        while (true) {
            process();
        }
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.yaleksandrova.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.yaleksandrova.tm.command.AbstractCommand.class)
                .stream()
                .sorted((o1, o2) -> o1.getPackage().getName().compareTo(o2.getPackage().getName()))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();

    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        @Nullable final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    public void  parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
    }

    private void process() {
        logService.debug("Test environment");
        @NotNull String command = "";
        while (!ApplicationConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = ApplicationUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Completed");
            } catch (@NotNull Exception e) {
                logService.error(e);
            }
        }
    }

    public void initUsers() {
        userService.create("TestUser", "test", "test_user@email.ru", Role.USER);
        userService.create("Admin", "admin", "admin@email.ru", Role.ADMIN);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
