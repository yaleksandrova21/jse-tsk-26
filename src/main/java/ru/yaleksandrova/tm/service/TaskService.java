package ru.yaleksandrova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.ITaskRepository;
import ru.yaleksandrova.tm.api.sevice.ITaskService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.empty.EmptyNameException;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.exception.system.IndexIncorrectException;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;
import java.util.Comparator;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty())  throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @NotNull
    @Override
    public Task findByName(@Nullable String userId, @Nullable String name) {
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(@Nullable String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task removeByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Task startById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(userId, id);
    }

    @NotNull
    @Override
    public Task startByIndex(@Nullable String userId, @Nullable Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0) throw new IndexIncorrectException();
        return taskRepository.startByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task startByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @NotNull
    @Override
    public Task finishById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @NotNull
    @Override
    public Task finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        return taskRepository.finishByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task finishByName(@Nullable String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @NotNull
    @Override
    public Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) return null;
        return taskRepository.changeStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (index == null) throw new EmptyIndexException();
        if(index < 0) throw new IndexIncorrectException();
        if (status == null) return null;
        return taskRepository.changeStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) return null;
        return taskRepository.changeStatusByName(userId, name, status);
    }

    @NotNull
    @Override
    public Task removeById(@Nullable String userId, @Nullable String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        taskRepository.remove(task);
        return task;
    }

    @Override
    public Integer size(String userId) {
        return taskRepository.size();
    }
}
